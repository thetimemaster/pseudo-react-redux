import * as PSR from './index';

const rows = ['some', 'other', 'content'];

const Row: PSR.PseudoReactComponent = ({text}) => ({
	tag: 'div',
	className: 'row',
	c: `Row: ${text}`,
});

const Content: PSR.PseudoReactComponent = ({}) => ({
	tag: 'section',
	className: 'main',
	c: rows.map((r) => Row({text: r})),
});

const BigObject: PSR.PseudoReactComponent = ({n, s = ''}) => ({
	tag: 'div',
	onClick: () => console.log(`${s}`),
	c:
		n === 1
			? s
			: Array.apply(null, Array(5)).map((el, i) =>
					BigObject({n: n - 1, s: `${s}${i}`}),
			  ),
});

const BigObjectWrapper: PSR.PseudoReactComponent = ({n}) => ({
	tag: 'div',
	c: [
		{
			tag: 'button',
			onClick: () => inc(),
			c: 'INC',
		},
		{
			tag: 'button',
			onClick: () => dec(),
			c: 'DEC',
		},
		BigObject({n}),
	],
});

enum ActionType {
	Initialize = 'Initialize',
	Dec = 'dec',
	Inc = 'inc',
}

type ExampleState = {
	size: number,
}

const defaultState = {
	size: 3,
};


const reducer: PSR.Reducer<ExampleState, PSR.IAction> = (state: ExampleState, action) => {
	switch (action.type) {
		case ActionType.Initialize:
			return defaultState;
		case ActionType.Dec:
			return {...state, size: state.size - 1};
		case ActionType.Inc:
			return {...state, size: state.size + 1};
		default:
			return state;
	}
};

PSR.Subscribe('content', (state) => ({}), Content);

PSR.Subscribe('root', (state: ExampleState) => ({n: state.size}), BigObjectWrapper);

PSR.InitializeApp(reducer);

const inc = () => PSR.Dispatch({type: ActionType.Inc});
const dec = () => PSR.Dispatch({type: ActionType.Dec});
