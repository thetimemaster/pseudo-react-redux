import {
	PseudoReactGenHTML,
	InsertToDOM,
	PseudoReactComponent,
	RenderOptimised,
	DEBUG,
} from './pseudo-react.js';

export interface IAction {
	type: string;
}

// In case something needs to exists in state in the future
// tslint:disable-next-line:no-empty-interface
export interface IState {}

export type Reducer<S extends IState, A extends IAction> = (state: S, action: A) => S;

export type RootRenderer<S extends IState> = {
	currentProps: any;
	isInitialised: boolean;
	rootElement: HTMLElement;
	mapStateToProps: (state: S) => any;
	elementFunction: PseudoReactComponent;
};

let _appReducer: Reducer<any, any> | null = null;
let _currentState: IState | null = null;
let _rootRenderers: RootRenderer<any>[] = [];

export const UpdateIfNeeded = (rootRenderer: RootRenderer<any>): RootRenderer<any> => {
	if (_currentState == null) return rootRenderer;

	const newProps = rootRenderer.mapStateToProps(_currentState);
	if (
		rootRenderer.currentProps === undefined ||
		newProps !== rootRenderer.currentProps
	) {
		if (rootRenderer.isInitialised) {
			RenderOptimised(
				rootRenderer.rootElement,
				rootRenderer.rootElement.lastChild as HTMLElement,
				rootRenderer.elementFunction(newProps),
			);
		} else {
			rootRenderer.isInitialised = true;
			InsertToDOM(
				rootRenderer.rootElement,
				PseudoReactGenHTML(rootRenderer.elementFunction(newProps)),
			);
		}

		rootRenderer.currentProps = newProps;
	}
	return rootRenderer;
};

export const UseState = () => _currentState;

export function Dispatch<A extends IAction>(action: A): void {
	if (_appReducer == null) return;

	_currentState = _appReducer(_currentState, action);
	_rootRenderers = _rootRenderers.map((r) => UpdateIfNeeded(r));

	if (DEBUG) console.log(_currentState);
}

// Must be called before InitializeApp
export function Subscribe<S extends IState>(
	rootId: string,
	mapStateToProps: (state: S) => any,
	elementFunction: PseudoReactComponent,
): void {
	const rootElem = document.getElementById(rootId);
	if (rootElem == null) return;

	const rootRenderer = {
		rootElement: rootElem,
		mapStateToProps,
		elementFunction,
		isInitialised: false,
		currentProps: undefined,
	};
	_rootRenderers.push(rootRenderer);
};

// Initializes state with {type: 'Initialize'} action. must be called before any Dispatch
export function InitializeApp<S extends IState, A extends IAction>(reducer: Reducer<S, A>) {
	_appReducer = reducer;
	Dispatch({type: 'Initialize'});
}
