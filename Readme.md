## About
This little framework is meant to the mimic behaviour of [React.js](https://reactjs.org/) + [Redux](https://redux.js.org/) with a few shortcuts:
* only function components are supported
* only root components support state to prop mapping - props in children have to be passed
directly from their parent
* JSX is non-existant, to avoid the need for additional compilation, instead HTML structures 
are represented by JSON objects with following structure:<br> 
```typescript
type PseudoReactRenderedComponent =
{
  tag: string           // HTML tag used to render the element
  className?: string    // HTML class attribute of the element
  id?: string           // HTML is of the element 
  isDisabled?: boolean  // should the HTML element have disabled='true' attribute
  onClick?: () => void  // 'click' listener of HTML element
  c?: PseudoReactRenderedComponent   |
      PseudoReactRenderedComponent[] |
      string            // child/children/innerText of HTML element
};

type PseudoReactComponent =
  (params: any) => PseudoReactRenderedComponent;
```
* as seen above, most of HTML attributes are not yet supported, as they were not needed in the target project
* re-render is primed based on actual HTML structure, not component props
## Motivation
#### Dynamic web-apps
Vanilla JS/TS is not well suited for creating dynamic web applications. 
It works well enough when you script a toggle button to open/close mobile menu, 
but in complex tasks, like generating bigger HTML structures on the fly, 
it becomes very clumsy. It has no inbuilt way of storing and copying nested components,
and creating just one HTML element needs few separate commands to: create it,
set the class, id and finally its place in the DOM.

That is why, over the time, many frameworks emerged to tackle the issue,
from which [React.js](https://reactjs.org/) is my personal favourite. Instead of thinking how the
HTML should change to be correct, it allows the user to concentrate 
what the current HTML should look like, by using JSX - an extension of js that allows to embed 
HTML structures as objects and pass them to the renderer, which processes the differences,
between last look and the new one and applies the changes.  

Smaller JSX structures can also be generated in the lesser component classes/functions and returned
to the bigger ones, to allow greater clarity of code and containerization. 
  
#### Functional programming
Mutable state seems nice to have, but is a real mess to maintain and test, especially when 
parts of that state are scattered across all the application and linked without any structure.
We can tackle the issue by decoupling state from presentation and stripping the visual elements
of any state whatsoever. Instead, the app visuals are a pure function of one bigger State.

But of course we don't want a static site, so we need to change the State somehow. Introducing Actions.
they are dispatched all over the app but instead of going straight to their target they go all the way up
to the main logic engine of the app - the Reducer - a function that given the State and an action returns
new State. That way we can be sure, that no element of the app stays unaware of some change made elsewhere.
Of course the Reducer does not mutate the old one but returns a new one. That is how we stay true to
our functional principals, though, frankly speaking, dispatching the action itself is a side effect,
but we can turn a blind eye to that.

Of course, once more, I did not come up with those ideas myself - this is the core functionality of [Redux](https://redux.js.org/).
#### Conclusion 
After some time using those two libraries, I could not imagine creating frontend app without them.
Then I was required to build one without ANY external libraries for my university course. That is why
I needed to recreate at least the core functionality of those two, so here it is - Pseudo-React-Redux.

## Usage
As the library is just meant to fill the absence of my core libraries in my university project, it has to 
obey it's rules:
* it was to be written in TS, so it needs to be TS-compiled before usage
* no additional compiler is required - after TSC it is just a set of files

## Examples
A very simple example app is provided with the package. More sophisticated example, frontend for the online quiz
hosting application, can be found in [this repository](https://gitlab.com/thetimemaster/quiz-hosting-platform)
