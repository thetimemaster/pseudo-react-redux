export const DEBUG = false;

const onClickHandlerMap = new Map();

const variousParams = ['type', 'placeholder'];

export type PseudoReactRenderedComponent = {
	tag: string;
	isDisabled?: boolean;
	onClick?: () => void;
	className?: string;
	id?: string;
	c?: PseudoReactRenderedComponent | PseudoReactRenderedComponent[] | string;
	// various string params
	type?: string;
	placeholder?: string;

	[key: string]: any;
};

export type PseudoReactComponent = (
	params: any,
) => PseudoReactRenderedComponent;

export const PseudoReactGenHTML = (
	structure: PseudoReactRenderedComponent,
): HTMLElement => {
	if (typeof structure !== 'object') {
		return (document.createTextNode(structure) as unknown) as HTMLElement;
	}

	const element = document.createElement(structure.tag);

	if (structure.className != null) element.className = structure.className;

	if (structure.id != null) element.id = structure.id;

	if (structure.onClick != null) {
		onClickHandlerMap.set(element, structure.onClick);
		element.addEventListener('click', structure.onClick);
	}

	if (structure.isDisabled === true) {
		element.setAttribute('disabled', 'true');
	}

	variousParams.forEach(param => {
		// @ts-ignore
		if (structure[param] != null) {
			// @ts-ignore
			element.setAttribute(param, structure[param]);
		}
	})

	if (structure.c != null) {
		if (Array.isArray(structure.c)) {
			structure.c.forEach((el) => {
				const renderedEl = PseudoReactGenHTML(el);
				element.appendChild(renderedEl);
			});
		} else if (typeof structure.c === 'object') {
			const renderedEl = PseudoReactGenHTML(structure.c);
			element.appendChild(renderedEl);
		} else {
			element.innerHTML = structure.c;
		}
	}

	return element;
};

export const RemoveHTMLElement = (
	parentElement: HTMLElement,
	element: HTMLElement,
): void => {
	ClearInnerDOM(element);
	if (onClickHandlerMap.has(element)) {
		onClickHandlerMap.delete(element);
	}
	parentElement.removeChild(element);
};

export const ClearInnerDOM = (element: HTMLElement): void => {
	while (element.firstChild) {
		RemoveHTMLElement(element, element.lastChild as HTMLElement);
	}
};

export const InsertToDOM = (
	rootElement: HTMLElement,
	renderedElement: HTMLElement,
): void => {
	ClearInnerDOM(rootElement);
	rootElement.appendChild(renderedElement);
};

export const ChildCount = (element: PseudoReactRenderedComponent): number => {
	if (typeof element === 'object') {
		if (element.c == null) return 0;
		if (Array.isArray(element.c)) return element.c.length;
		return 1;
	} else {
		return 0;
	}
};

const UndefToEmptyString = (s: string | undefined | null): string => {
	return s == null ? '' : s;
};

const Arrayize = (obj: any) => {
	if (obj == null) return [];
	if (Array.isArray(obj)) return obj;
	return [obj];
};

// Checks if DOM element can represent root of renderedComponent
export const CheckRootEquality = (
	htmlElement: HTMLElement,
	elementTree: PseudoReactRenderedComponent,
): boolean => {
	if (typeof elementTree !== 'object') {
		if (DEBUG)
			console.log(
				`text node found: '${htmlElement.textContent}' '${elementTree}'`,
			);

		return htmlElement.textContent === elementTree;
	}

	// For the sake of simplicity it is also false if child count differs
	if (htmlElement.childNodes.length !== ChildCount(elementTree)) {
		if (DEBUG)
			console.log(
				`mismatched child count: ${
					htmlElement.childNodes.length
				} ${ChildCount(elementTree)}`,
			);
		return false;
	}
	if (htmlElement.tagName.toUpperCase() !== elementTree.tag.toUpperCase()) {
		if (DEBUG)
			console.log(
				`mismatched tag: '${htmlElement.tagName}' '${elementTree.tag}'`,
			);
		return false;
	}

	for (const param of variousParams) {
		if (UndefToEmptyString(htmlElement.getAttribute(param)) !== UndefToEmptyString(elementTree[param])) {
			if (DEBUG)
				console.log(
					`mismatched ${param}: '${UndefToEmptyString(htmlElement.getAttribute(param))}' '${UndefToEmptyString(elementTree[param])}'`,
				);

			htmlElement.setAttribute(param, UndefToEmptyString(elementTree.className));
		}
	}

	if (htmlElement.className !== UndefToEmptyString(elementTree.className)) {
		if (DEBUG)
			console.log(
				`mismatched className: '${htmlElement.className}' '${UndefToEmptyString(elementTree.className)}'`,
			);

		htmlElement.className = UndefToEmptyString(elementTree.className);
	}

	if (htmlElement.id !== UndefToEmptyString(elementTree.id)) {
		if (DEBUG)
			console.log(
				`mismatched ID: '${htmlElement.id}' '${UndefToEmptyString(elementTree.id)}'`,
			);

		htmlElement.id = UndefToEmptyString(elementTree.id);
	}

	if (
		(htmlElement.hasAttribute('disabled') &&
			elementTree.isDisabled !== true) ||
		(!htmlElement.hasAttribute('disabled') &&
			elementTree.isDisabled === true)
	)
		return false;

	// We have no way to check if onclick functions are equal so we always
	// override them
	if (onClickHandlerMap.has(htmlElement)) {
		htmlElement.removeEventListener(
			'click',
			onClickHandlerMap.get(htmlElement),
		);
	}
	if (elementTree.onClick != null) {
		onClickHandlerMap.set(htmlElement, elementTree.onClick);
		htmlElement.addEventListener('click', elementTree.onClick);
	}

	return true;
};

export const RenderOptimised = (
	parentElement: HTMLElement,
	currentElement: HTMLElement,
	newTree: PseudoReactRenderedComponent,
): void => {
	if (CheckRootEquality(currentElement, newTree)) {
		const children = Arrayize(newTree.c);
		currentElement.childNodes.forEach((child, i) => {
			RenderOptimised(currentElement, child as HTMLElement, children[i]);
		});
	} else {
		const newTreeHTML = PseudoReactGenHTML(newTree);
		parentElement.insertBefore(newTreeHTML, currentElement);
		RemoveHTMLElement(parentElement, currentElement);
	}
};
